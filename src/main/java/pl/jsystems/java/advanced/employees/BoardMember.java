package pl.jsystems.java.advanced.employees;

public class BoardMember extends Manager {
    public BoardMember(String name) {
        super(name);
    }

    public BoardMember(String name, int salary, String city) {
        super(name, salary, city);
    }
}
