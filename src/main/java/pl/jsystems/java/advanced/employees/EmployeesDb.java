package pl.jsystems.java.advanced.employees;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class EmployeesDb {
    private EmployeesDb() {
        throw new AssertionError("No instance allowed!");
    }

    public static List<Employee> getEmployees() {
        return Arrays.asList(
                new Employee("Jan Kowalski", 3000, "Warszawa"),
                new Employee("Małgorzata Mróz", 3500, "Warszawa"),
                new Employee("Grzegorz Hyra", 4000, "Warszawa"),
                new Employee("Adrian Dolny", 7000, "Warszawa"),
                new Employee("Agnieszka Górna", 3000, "Wrocław"),
                new Employee("Ewa Jankowska", 3000, "Gdańsk"),
                new Manager("Dominik Okrężny", 7000, "Warszawa"),
                new Manager("Gabriela Niemiecka", 7500, "Wrocław"),
                new Manager("Leon Skwarek", 9000, "Warszawa"),
                new Manager("Tadeusz Pyra", 8500, "Warszawa"),
                new Director("Ewa Środa", 10000, "Gdańsk"),
                new Director("Piotr Rybak", 12000, "Warszawa"),
                new BoardMember("Dagmara Markowska", 15000, "Gdańsk"),
                new BoardMember("Ireneusz Takiewicz", 15000, "Warszawa")
        );
    }

    public static List<Manager> getManagers() {
        return getEmployees().stream()
                .filter(e -> Manager.class.isAssignableFrom(e.getClass()))
                .map(e -> (Manager) e)
                .collect(Collectors.toList());
    }

    public static List<Director> getDirectors() {
        return getEmployees().stream()
                .filter(e -> Director.class.isAssignableFrom(e.getClass()))
                .map(e -> (Director) e)
                .collect(Collectors.toList());
    }

    public static List<BoardMember> getBoardMembers() {
        return getEmployees().stream()
                .filter(e -> BoardMember.class.isAssignableFrom(e.getClass()))
                .map(e -> (BoardMember) e)
                .collect(Collectors.toList());
    }
}
