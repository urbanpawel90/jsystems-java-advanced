package pl.jsystems.java.advanced.employees;

public class Manager extends Employee {
    public Manager(String name) {
        super(name);
    }

    public Manager(String name, int salary, String city) {
        super(name, salary, city);
    }
}
