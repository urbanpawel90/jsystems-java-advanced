package pl.jsystems.java.advanced.employees;

public class Director extends Manager {
    public Director(String name) {
        super(name);
    }

    public Director(String name, int salary, String city) {
        super(name, salary, city);
    }
}
