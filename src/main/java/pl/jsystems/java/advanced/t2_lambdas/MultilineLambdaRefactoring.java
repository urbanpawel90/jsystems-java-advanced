package pl.jsystems.java.advanced.t2_lambdas;

import pl.jsystems.java.advanced.employees.EmployeesDb;
import pl.jsystems.java.advanced.employees.Manager;

public class MultilineLambdaRefactoring {
    public static void run() {
        EmployeesDb.getEmployees().stream()
                .filter(employee -> {
                    if (!(employee instanceof Manager)) {
                        return false;
                    }
                    if (employee.getSalary() >= 8000) {
                        return false;
                    }
                    return true;
                })
                .forEach(System.out::println);
    }
}
