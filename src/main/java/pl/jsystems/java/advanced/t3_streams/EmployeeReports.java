package pl.jsystems.java.advanced.t3_streams;

import pl.jsystems.java.advanced.employees.Employee;

import java.util.List;

public class EmployeeReports {

    public static List<Employee> findFromWroclaw() {
        return null;
    }

    public static List<String> findCities() {
        return null;
    }

    public static List<String> findFemaleNames() {
        return null;
    }

    public static int findMinimumSalary() {
        return 0;
    }

    public static int countEmployeesOutsideWroclaw() {
        return 0;
    }

    public static List<Employee> findAllExceptManagers() {
        return null;
    }

    public static List<Employee> findTop5EarningEmployees() {
        return null;
    }

    public static List<Employee> findEmployeesWithTheLowestSalary() {
        return null;
    }
}
