package pl.jsystems.java.advanced.t3_streams;

import org.junit.Test;
import pl.jsystems.java.advanced.employees.Employee;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class T3E1 {
    @Test
    public void test_employeesAreOnlyFromWroclaw() {
        List<Employee> employeesFromWroclaw = EmployeeReports.findFromWroclaw();
        assertThat(employeesFromWroclaw)
                .allMatch(e -> e.getCity().equals("Wrocław"));
    }
}
