package pl.jsystems.java.advanced.t3_streams;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class T3E2 {
    @Test
    public void test_allCitiesShouldBeDistinctlyFound() {
        List<String> cities = EmployeeReports.findCities();
        
        assertThat(cities).doesNotHaveDuplicates()
                .containsExactlyInAnyOrder("Wrocław", "Gdańsk", "Warszawa");
    }
}
