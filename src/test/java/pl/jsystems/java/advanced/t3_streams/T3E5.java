package pl.jsystems.java.advanced.t3_streams;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class T3E5 {
    @Test
    public void test_properlyCountsPeopleOutsideWroclaw() {
        assertThat(EmployeeReports.countEmployeesOutsideWroclaw()).isEqualTo(12);
    }
}
