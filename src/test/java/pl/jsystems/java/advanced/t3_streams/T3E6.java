package pl.jsystems.java.advanced.t3_streams;

import org.junit.Test;
import pl.jsystems.java.advanced.employees.Manager;

import static org.assertj.core.api.Assertions.assertThat;

public class T3E6 {
    @Test
    public void test_findAllNonManagers() {
        assertThat(EmployeeReports.findAllExceptManagers()).allMatch(e -> e.getClass() != Manager.class);
    }
}
