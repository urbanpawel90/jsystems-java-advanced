package pl.jsystems.java.advanced.t3_streams;

import org.junit.Test;
import pl.jsystems.java.advanced.employees.Director;
import pl.jsystems.java.advanced.employees.Employee;
import pl.jsystems.java.advanced.employees.Manager;

import java.util.Comparator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class T3E7 {
    @Test
    public void test_findTop5BestEarningEmployeesExceptBoardMemebers() {
        List<Employee> top5Salaries = EmployeeReports.findTop5EarningEmployees();

        assertThat(top5Salaries)
                .usingElementComparator(Comparator.comparing(Employee::getName).thenComparing(Employee::getSalary))
                .containsExactly(
                        new Director("Piotr Rybak", 12000, "Warszawa"),
                        new Director("Ewa Środa", 10000, "Gdańsk"),
                        new Manager("Leon Skwarek", 9000, "Warszawa"),
                        new Manager("Tadeusz Pyra", 8500, "Warszawa"),
                        new Manager("Gabriela Niemiecka", 7500, "Wrocław")
                );
    }
}
