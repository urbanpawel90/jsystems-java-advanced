package pl.jsystems.java.advanced.t3_streams;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class T3E3 {
    @Test
    public void test_findsDistinctFemaleNames() {
        List<String> names = EmployeeReports.findFemaleNames();

        assertThat(names)
                .doesNotHaveDuplicates()
                .containsExactlyInAnyOrder("Małgorzata", "Agnieszka", "Ewa", "Gabriela", "Dagmara");
    }
}
