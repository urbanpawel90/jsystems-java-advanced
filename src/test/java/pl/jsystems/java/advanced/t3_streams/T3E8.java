package pl.jsystems.java.advanced.t3_streams;

import org.junit.Test;
import pl.jsystems.java.advanced.employees.Employee;

import java.util.Comparator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class T3E8 {
    @Test
    public void test_findsAllEmployeesWithLowestEarnings() {
        List<Employee> lowestSalaryEmployees = EmployeeReports.findEmployeesWithTheLowestSalary();

        assertThat(lowestSalaryEmployees)
                .usingElementComparator(Comparator.comparing(Employee::getName).thenComparing(Employee::getSalary))
                .containsExactlyInAnyOrder(
                        new Employee("Jan Kowalski", 3000, "Warszawa"),
                        new Employee("Agnieszka Górna", 3000, "Wrocław"),
                        new Employee("Ewa Jankowska", 3000, "Gdańsk")
                );
    }
}
