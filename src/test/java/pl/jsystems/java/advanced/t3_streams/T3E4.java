package pl.jsystems.java.advanced.t3_streams;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class T3E4 {
    @Test
    public void test_findMinimumSalary() {
        assertThat(EmployeeReports.findMinimumSalary()).isEqualTo(3000);
    }

    @Test
    public void test_findMaximumSalary() {
        assertThat(EmployeeReports.findMinimumSalary()).isEqualTo(15000);
    }
}
