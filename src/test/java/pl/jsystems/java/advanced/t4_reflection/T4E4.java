package pl.jsystems.java.advanced.t4_reflection;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class T4E4 {
    private FormatterRegistry registry;

    @Before
    public void setUp() {
        registry = new FormatterRegistry();
    }

    @Test
    public void formatterRegistry_allowsToRegisterFormatter() {
        registry.registerFormatter(() -> "");
    }

    @Test(expected = FormatterRegistry.DuplicateFormatterException.class)
    public void formatterRegistry_doesNotAllowToRegisterFormatterForTheSameType() {
        registry.registerFormatter(() -> "");
        registry.registerFormatter(() -> "");
    }

    @Test
    public void formatterRegistry_canFindRegisteredFormatter() {
        registry.registerFormatter(() -> "");

        assertNotNull(registry.findFormatter(/* Implement */));
    }

    @Test
    public void formatterRegistry_doesNotReturnUnregisteredFormatter() {
        assertNull(registry.findFormatter(/* Implement */));
    }

    @Test
    public void formatterRegistry_willReturnSuperclassFormatterIfNotFoundForClass() {
        // TODO: Implement
        // Remember to search only for superclasses, not extended interfaces
    }

    @Test
    public void formatterRegistry_willNotReturnExtendedInterfaceFormatterIfNotFoundForClass() {
        // TODO: Implement
    }
}
