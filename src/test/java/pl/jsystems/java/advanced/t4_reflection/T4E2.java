package pl.jsystems.java.advanced.t4_reflection;

import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class T4E2 {
    private ByteArrayOutputStream systemOut;

    private String readSystemOut() {
        return new BufferedReader(
                new InputStreamReader(
                        new ByteArrayInputStream(systemOut.toByteArray())))
                .lines()
                .collect(Collectors.joining(System.lineSeparator()));
    }

    @Before
    public void setUp() {
        systemOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(systemOut));
    }

    @Test
    public void test_printInheritanceHierarchy() {
        ReflectionUtils.printBoardMemeberInheritanceHierarchy();

        assertThat(readSystemOut()).isEqualToNormalizingNewlines(
                "pl.jsystems.java.advanced.employees.BoardMember\n" +
                        "pl.jsystems.java.advanced.employees.Manager\n" +
                        "pl.jsystems.java.advanced.employees.Employee\n" +
                        "java.lang.Object"
        );
    }
}
