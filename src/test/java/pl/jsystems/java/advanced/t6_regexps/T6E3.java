package pl.jsystems.java.advanced.t6_regexps;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class T6E3 {
    @Test
    public void test_shouldNotAllowDigitAsFirstUsernameCharacter() {
        assertThat(Regexps.validateEmail("1abc@example.com")).isFalse();
    }

    @Test
    public void test_shouldNotAllowDigitAsFirstDomainCharacter() {
        assertThat(Regexps.validateEmail("john.doe@1example.com")).isFalse();
    }

    @Test
    public void test_shouldNotAllowEmptyUsernamePart() {
        assertThat(Regexps.validateEmail("@example.com")).isFalse();
    }

    @Test
    public void test_shouldNotAllowEmptyDomainPart() {
        assertThat(Regexps.validateEmail("john.doe@")).isFalse();
    }

    @Test
    public void test_shouldNotAllowDomainWithoutDot() {
        assertThat(Regexps.validateEmail("john.doe@example")).isFalse();
    }

    @Test
    public void test_shouldNotAllowWhitespaceInUsername() {
        assertThat(Regexps.validateEmail("john doe@example.com")).isFalse();
    }

    @Test
    public void test_shouldNotAllowWhitespaceInDomain() {
        assertThat(Regexps.validateEmail("john.doe@example .com")).isFalse();
    }

    @Test
    public void test_shouldAllowSimpleEmail() {
        assertThat(Regexps.validateEmail("johndoe90@example.com")).isTrue();
    }

    @Test
    public void test_shouldAllowUsernameWithDot() {
        assertThat(Regexps.validateEmail("john.doe@example.com")).isTrue();
    }

    @Test
    public void test_shouldAllowDomainWithMoreThatOneDot() {
        assertThat(Regexps.validateEmail("john.doe@example.company.com")).isTrue();
    }

    @Test
    public void test_shouldAllowDashInUsername() {
        assertThat(Regexps.validateEmail("john-doe@example.com")).isTrue();
    }

    @Test
    public void test_shouldAllowDashInDomain() {
        assertThat(Regexps.validateEmail("john.doe@example-company.com")).isTrue();
    }
}
