package pl.jsystems.java.advanced.t6_regexps;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class T6E2 {
    @Test
    public void test_shouldReturnFalseOnLowerCaseStart() {
        assertThat(Regexps.startsWithLowerCaseOrDigit("abc")).isFalse();
    }

    @Test
    public void test_shouldReturnFalseOnWhiteSpaceStart() {
        assertThat(Regexps.startsWithLowerCaseOrDigit(" Abc")).isFalse();
        assertThat(Regexps.startsWithLowerCaseOrDigit(" abc")).isFalse();
        assertThat(Regexps.startsWithLowerCaseOrDigit(" 12")).isFalse();
    }

    @Test
    public void test_shouldReturnFalseWhenNotFinishedWithDot() {
        assertThat(Regexps.startsWithLowerCaseOrDigit("Abc;")).isFalse();
        assertThat(Regexps.startsWithLowerCaseOrDigit("Abc ")).isFalse();
        assertThat(Regexps.startsWithLowerCaseOrDigit("Abc")).isFalse();
    }

    @Test
    public void test_shouldReturnTrueWhenStartsWithUpperCaseAndEndsWithDot() {
        assertThat(Regexps.startsWithLowerCaseOrDigit("Abc def.")).isTrue();
    }
}
