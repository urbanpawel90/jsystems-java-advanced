package pl.jsystems.java.advanced.t6_regexps;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class T6E1 {
    @Test
    public void test_shouldReturnFalseOnLowerCase() {
        assertThat(Regexps.areUpperCaseWords("ala ma kota")).isFalse();
        assertThat(Regexps.areUpperCaseWords("ala")).isFalse();
    }

    @Test
    public void test_shouldReturnFalseOnDigits() {
        assertThat(Regexps.areUpperCaseWords("123")).isFalse();
        assertThat(Regexps.areUpperCaseWords("123 000")).isFalse();
    }

    @Test
    public void test_shouldReturnFalseOnMixedDigitsAndUpperCase() {
        assertThat(Regexps.areUpperCaseWords("123ABC")).isFalse();
        assertThat(Regexps.areUpperCaseWords("123 ABC")).isFalse();
    }

    @Test
    public void test_shouldReturnTrueOnUpperCaseWords() {
        assertThat(Regexps.areUpperCaseWords("ALA MA KOTA")).isTrue();
        assertThat(Regexps.areUpperCaseWords("ALA")).isTrue();
    }
}
