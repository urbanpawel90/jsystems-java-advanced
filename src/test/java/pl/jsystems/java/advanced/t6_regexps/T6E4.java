package pl.jsystems.java.advanced.t6_regexps;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class T6E4 {
    @Test
    public void test_shouldReturnEmptyListWhenNoFemaleNamesInText() {
        assertThat(Regexps.findFemaleNames("Lorem ipsum dolor amet")).isEmpty();
    }

    @Test
    public void test_shouldReturnDistinctNamesIfNameRepeatedInText() {
        assertThat(Regexps.findFemaleNames("Basia wyszła z domu na rozmowę. Basia dostała tą pracę!"))
                .containsExactly("Basia");
    }

    @Test
    public void test_shouldFindAllFemaleNamesInText() {
        assertThat(Regexps.findFemaleNames("Basia, Gosia i Ewelina poszły na przerwę, natomiast Ola miała jeszcze parę rzeczy do zrobienia."))
                .containsExactlyInAnyOrder("Basia", "Gosia", "Ewelina", "Ola");
    }
}
