package pl.jsystems.java.advanced.t1_generics;

import org.junit.Test;
import pl.jsystems.java.advanced.employees.Employee;
import pl.jsystems.java.advanced.employees.EmployeesDb;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class T1E2 {
    @Test
    public void test_shouldGiveRaiseToAllEmployees() {
        List<Employee> employees = EmployeesDb.getEmployees();
        int firstSalaryBeforeRaise = employees.get(0).getSalary();
        int lastSalaryBeforeRaise = employees.get(employees.size() - 1).getSalary();
//        RaiseUseCase.giveRaise(employees);

        assertEquals(employees.get(0).getSalary(), (int) (firstSalaryBeforeRaise * 1.2));
        assertEquals(employees.get(employees.size() - 1).getSalary(), (int) (lastSalaryBeforeRaise * 1.2));
    }

    // TODO: Uzupelnij testy dla pozostalych list: List<Manager>, List<Director>, List<BoardMember>
    @Test
    public void test_shouldGiveRaiseToAllManagers() {
        fail("Brak implementacji");
    }

    @Test
    public void test_shouldGiveRaiseToAllDirectors() {
        fail("Brak implementacji");
    }

    @Test
    public void test_shouldGiveRaiseToAllBoardMembers() {
        fail("Brak implementacji");
    }
}
