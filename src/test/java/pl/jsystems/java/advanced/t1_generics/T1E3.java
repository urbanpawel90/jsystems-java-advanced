package pl.jsystems.java.advanced.t1_generics;

import org.junit.Test;
import pl.jsystems.java.advanced.employees.Employee;
import pl.jsystems.java.advanced.employees.Manager;

import static org.junit.Assert.*;

public class T1E3 {
    @Test
    public void test_givenAllStringsShouldReturnStringArray() {
        String[] result = null;// ArrayUtils.createArray("a", "b", "c");

        assertEquals(result.length, 3);
        assertArrayEquals(result, new String[]{"a", "b", "c"});
    }

    @Test
    public void test_givenAllIntegersShouldReturnIntegerArray() {
        fail("Not implemented");
    }

    @Test
    public void test_givenEmployeeAndManagerShouldReturnEmployeeArray() {
        Employee employee = new Employee("Marek");
        Manager manager = new Manager("Dawid");
        Employee[] result = null;// ArrayUtils.createArray(employee, manager);

        assertEquals(result.length, 2);
        assertEquals(result.getClass().getComponentType(), Employee.class);
        assertArrayEquals(result, new Employee[]{employee, manager});
    }

    @Test
    public void test_givenBoardMemberAndDirectorShouldReturnXXX() {
        // TODO: Uzupelnij nazwe testy poprawnym typem zwracanej tablicy
        fail("Not implemented");
    }
}
