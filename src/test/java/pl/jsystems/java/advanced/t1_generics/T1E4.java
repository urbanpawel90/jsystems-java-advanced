package pl.jsystems.java.advanced.t1_generics;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class T1E4 {
    @Test
    public void test_shouldSwapStringsInArray() {
        String[] swapped = null; //ArrayUtils.swap(new String[]{"a", "b", "c", "d", "e"}, 1, 4);

        assertEquals(swapped.length, 5);
        assertEquals(swapped[1], "e");
        assertEquals(swapped[4], "b");
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void test_shouldThrowWhenStartIndexOutOfRange() {
//        ArrayUtils.swap(new String[]{"a"}, 2, 3);
    }

    @Test
    public void test_shouldThrowWhenEndIndexOutOfRange() {
        fail("Not implemented");
    }

    @Test
    public void test_shouldSwapIntegersInArray() {
        fail("Not implemented");
    }

    @Test
    public void test_shouldSwapEmployeesInArray() {
        fail("Not implemented");
    }
}
