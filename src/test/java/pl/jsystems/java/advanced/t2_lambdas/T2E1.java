package pl.jsystems.java.advanced.t2_lambdas;

import org.junit.Test;
import pl.jsystems.java.advanced.employees.Employee;
import pl.jsystems.java.advanced.employees.EmployeesDb;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class T2E1 {
    @Test
    public void test_shouldApplySalaryFunctionToAllEmployees() {
        List<Employee> employees = EmployeesDb.getEmployees();
        List<Integer> expectedSalaries = employees.stream()
                .map(Employee::getSalary)
                .map(s -> (int) (s * 1.5))
                .collect(Collectors.toList());

//        CustomRaiseUseCase.giveRaise(employees, emp -> (int) (emp.getSalary() * 1.5));

        assertEquals(expectedSalaries, employees.stream().map(Employee::getSalary).collect(Collectors.toList()));
    }
}
