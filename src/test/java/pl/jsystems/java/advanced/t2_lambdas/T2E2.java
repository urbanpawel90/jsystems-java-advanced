package pl.jsystems.java.advanced.t2_lambdas;

import org.junit.Test;
import pl.jsystems.java.advanced.employees.Employee;
import pl.jsystems.java.advanced.employees.EmployeesDb;
import pl.jsystems.java.advanced.employees.Manager;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class T2E2 {
    @Test
    public void test_shouldApplyRaiseFunctionToAllEmployees() {
        List<Employee> employees = EmployeesDb.getEmployees();
        List<Integer> expectedSalaries = employees.stream()
                .map(Employee::getSalary)
                .map(s -> (int) (s * 1.5))
                .collect(Collectors.toList());

//        CustomRaiseUseCase.giveRaiseE2(employees, emp -> (int) (emp.getSalary() * 1.5));

        assertEquals(expectedSalaries, employees.stream().map(Employee::getSalary).collect(Collectors.toList()));
    }

    @Test
    public void test_shouldApplyRaiseFunctionToAllManagers() {
        List<Manager> employees = EmployeesDb.getManagers();
        List<Integer> expectedSalaries = employees.stream()
                .map(Employee::getSalary)
                .map(s -> (int) (s * 1.5))
                .collect(Collectors.toList());

//        CustomRaiseUseCase.giveRaiseE2(employees, emp -> (int) (emp.getSalary() * 1.5));

        assertEquals(expectedSalaries, employees.stream().map(Employee::getSalary).collect(Collectors.toList()));
    }

    @Test
    public void test_shouldApplyRaiseFunctionToAllDirectors() {
        fail("Not implemented");
    }

    @Test
    public void test_shouldApplyRaiseFunctionToAllBoardMemebers() {
        fail("Not implemented");
    }
}
